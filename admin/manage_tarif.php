<!DOCTYPE html>
<html>
<head>
   
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Admin E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="../assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../assets/js/dataTables/dataTables.bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../css/style.css">
 
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Admin E-PLN</a> 
            </div>
            <div style="color: white;
            padding: 15px 50px 5px 50px;
            float: right;
            font-size: 16px;"><a href="logout.php" class="btn btn-info square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
        </nav>   
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li class="text-center">
                        <img src="../img/log.png" class="user-image img-responsive"/>
                    </li>
                    <li>
                        <a href="index.php"><i class="fa fa-home fa-2x"></i> Home</a>
                    </li>
                    <li>
                        <a  href="manage_user.php"><i class="fa fa-user fa-2x"></i>Manage User</a>
                    </li>
                    <li>
                        <a class="active-menu" href="manage_tarif.php"><i class="fa fa-user fa-2x"></i>Manage Tarif</a>
                    </li>
                    <li>
                        <a  href="verifikasi.php"><i class="fa fa-qrcode fa-2x"></i>Verifikasi & Validasi</a>
                    </li>
                    <li>
                        <a  href="laporan.php"><i class="fa fa-qrcode fa-2x"></i>Laporan</a>
                    </li>   
                </ul>

            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">  
                       <h4 align="center">Pengelola Data Pelanggan</h4>
                       <div class="container">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#tambah_tarif
                        ">Tambah Data</button>

                        <!-- Modal -->
                        <div class="modal fade" id="tambah_tarif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Tambah Tarif</h4>
                              </div>
                              <div class="modal-body">
                              <form action="tambah_tarif.php" method="POST">
                                <div class="form-group">
                                  <label>Daya</label>
                                  <input type="text" class="form-control" placeholder="enter daya" name="daya" required>
                                </div>
                                <div class="form-group">
                                  <label>Tarif Perkwh</label>
                                  <input type="text" class="form-control" placeholder="enter tarif perkwh" name="tarifperkwh" required>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="simpan" class="btn btn-primary">Save changes</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                              </form>
                              </div>
                            </div>
                          </div>
                        </div>
                       </div>

                       <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="example">
                                    <thead>
                                        <tr>
                                            <th>Daya</th>
                                            <th>Tarif Perkwh</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php

                                        include '../koneksi.php';
                                        $no = 1;
                                        $tampil = mysqli_query($koneksi, "select * from tarif");
                                        while($data = mysqli_fetch_array($tampil)){

                                      ?>
                                      <tr>
                                        <td><?php echo $data['daya']; ?></td>
                                        <td><?php echo $data['tarifperkwh']; ?></td>
                                        <td>
                                          <button type="button" class="btn btn-info"><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#edit_tarif-<?php echo $data['id_tarif'];?>"></span></button>
                                          <a href="hapus_tarif.php?id_tarif=<?php echo $data['id_tarif']; ?>"><button type="button" class="btn btn-info"> <span class="glyphicon glyphicon-trash"></span></button></a>
                                        </td>
                                      </tr>


                                  <!-- Modal -->
                                  <div class="modal fade" id="edit_tarif-<?php echo $data['id_tarif'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">Edit Tarif</h4>
                                        </div>
                                        <div class="modal-body">
                                        <form action="edit_tarif.php" method="POST">
                                          <div class="form-group">
                                            <label>Daya</label>
                                            <input type="hidden" name="id_tarif" value="<?php echo $data['id_tarif']; ?>">
                                            <input type="text" class="form-control" placeholder="enter daya" name="daya" value="<?php echo $data['daya'];?>">
                                          </div>
                                          <div class="form-group">
                                            <label>Tarif Perkwh</label>
                                            <input type="text" class="form-control" placeholder="enter tarif perkwh" name="tarifperkwh" value="<?php echo $data['tarifperkwh'];?>">
                                          </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="submit" name="simpan" class="btn btn-primary">Save changes</button>
                                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        </form>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                      <?php } ?>

                                    </tbody>
                                  </table>

                            </div>
                        </div>
                   </div>
               </div>

               <!-- /. ROW  -->
               <hr />
               
           </div>
           <!-- /. PAGE INNER  -->
       </div>
       <!-- /. PAGE WRAPPER  -->
   </div>
   <!-- /. WRAPPER  -->
   <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
   <!-- JQUERY SCRIPTS -->
   <script src="../assets/js/jquery-1.10.2.js"></script>
   <!-- BOOTSTRAP SCRIPTS -->
   <script src="../assets/js/bootstrap.min.js"></script>
   <!-- METISMENU SCRIPTS -->
   <script src="../assets/js/jquery.metisMenu.js"></script>
   <!-- DATA TABLE SCRIPTS -->
   <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
   <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
   <!-- CUSTOM SCRIPTS -->
   <script>
            $(document).ready(function () {
                $('#example').dataTable();
            });
    </script>
   <script src="../assets/js/custom.js"></script>

   
</body>
</html>
