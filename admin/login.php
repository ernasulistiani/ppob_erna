 <html> 
 <head> 
  <title>E-PLN</title> 
  <!-- BOOTSTRAP STYLES-->
  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <link href="../stylelogin.css" rel="stylesheet" type="text/css" > 
</head> 

<body> 
  <?php 
  if(isset($_GET['pesan'])){
    if($_GET['pesan']=="gagal"){
      echo "<div class='alert'>Username dan Password tidak sesuai !</div>";
    }
  }
  ?>

  <div id="utama"> 
    <div class="header"> 
      <form action="proses_login.php" method="POST" class="form-header"> 
        <img src="../img/log.png" ><br /><br /> 

        <input type="text" name="username" placeholder="Username" class="login_regis">     <br /> 
        <input type="password" name="password" placeholder="Password" class="login_regis"> <br /> 

        <input type="Submit" name="login" value="LOGIN" class="tombol_login"></br></br>
        <p align="center">Belum punya akun? <a href="" data-toggle="modal" data-target="#registrasi">Daftar Sekarang</a></p>
        <p align="center"><a href="#">Lupa kata sandi?</a></p></br>
      </form> 
    </div>

    <!-- Modal -->
    <div class="modal fade" id="registrasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Registrasi Akun Anda</h4>
          </div>
          <div class="modal-body">
            <form action="regis.php" method="POST">
              <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" placeholder="Username" name="username" required>
              </div>
              <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" placeholder="Password" name="password" required>
              </div>
              <div class="form-group">
                <label>Nomor Kwh</label>
                <input type="text" class="form-control" placeholder="Nomor Kwh" name="nomor_kwh" required>
              </div>
              <div class="form-group">
                <label>Nama Pelanggan</label>
                <input type="text" class="form-control" placeholder="Nama Pelanggan" name="nama_pelanggan" required>
              </div>
              <div class="form-group">
                <label>Alamat</label>
                <input type="text" class="form-control" placeholder="Alamat" name="alamat" required>
              </div>
              <div class="form-group">
                <label>Id Tarif</label>
                <input type="text" class="form-control" placeholder="Id Tarif" name="id_tarif" required>
              </div>
            </form>
            <div class="modal-footer">
              <button type="submit" name="simpan" class="btn btn-primary">Save changes</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- JQUERY SCRIPTS -->
    <script src="../assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="../assets/js/bootstrap.min.js"></script>
  </body> 
  </html>