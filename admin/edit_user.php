<!DOCTYPE html>
<html>
<head>
   
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Admin E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="../assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Admin E-PLN</a> 
            </div>
            <div style="color: white;
            padding: 15px 50px 5px 50px;
            float: right;
            font-size: 16px;"><a href="logout.php" class="btn btn-info square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
        </nav>   
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li class="text-center">
                        <img src="../img/log.png" class="user-image img-responsive"/>
                    </li>
                    <li>
                        <a href="index.php"><i class="fa fa-home fa-2x"></i> Home</a>
                    </li>
                    <li>
                        <a class="active-menu" href="manage_user.php"><i class="fa fa-user fa-2x"></i>Manage User</a>
                    </li>
                    <li>
                        <a  href="manage_tarif.php"><i class="fa fa-user fa-2x"></i>Manage Tarif</a>
                    </li>
                    <li>
                        <a  href="verifikasi.php"><i class="fa fa-qrcode fa-2x"></i>Verifikasi & Validasi</a>
                    </li>
                    <li>
                        <a  href="laporan.php"><i class="fa fa-qrcode fa-2x"></i>Laporan</a>
                    </li> 
                </ul>

            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Edit User
                        </div>
                        <div class="panel-body">
                            <div class="row">

                       
                       <?php
                          include "../koneksi.php";
                          $id_pelanggan=$_GET['id_pelanggan'] ;
                          $nama = mysqli_query($koneksi, "SELECT * from pelanggan where id_pelanggan='$id_pelanggan'");
                          $data = mysqli_fetch_array($nama);
                        ?>

                              <form method="POST">
                                <div class="col-md-6">
                                <div class="form-group">
                                  <label>Username</label>
                                  <input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo $data['username'];?>">
                                </div>
                                <div class="form-group">
                                  <label>Password</label>
                                  <input type="password" class="form-control" placeholder="Password" name="password" value="<?php echo $data['password'];?>">
                                </div>
                                <div class="form-group">
                                  <label>Nomor Kwh</label>
                                  <input type="text" class="form-control" placeholder="Nomor Kwh" name="nomor_kwh" value="<?php echo $data['nomor_kwh'];?>">
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                  <label>Nama Pelanggan</label>
                                  <input type="text" class="form-control" placeholder="Nama Pelanggan" name="nama_pelanggan" value="<?php echo $data['nama_pelanggan'];?>">
                                </div>
                                <div class="form-group">
                                  <label>Alamat</label>
                                  <input type="text" class="form-control" placeholder="Alamat" name="alamat" value="<?php echo $data['alamat'];?>">
                                </div>
                                <div class="form-group">
                                  <label>Id Tarif</label>
                                  <input type="text" class="form-control" placeholder="Id Tarif" name="id_tarif" value="<?php echo $data['id_tarif'];?>">
                                </div>
                                <button type="submit" class="btn btn-info" name="simpan">Submit</button>
								                <a href="manage_user.php"><button type="button" class="btn btn-danger">Cancel</button></a>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>

                      <?php

                          if(isset($_POST['simpan'])){
                          $username=$_POST['username'];
                          $password=$_POST['password'];
                          $nomor_kwh=$_POST['nomor_kwh'];
                          $nama_pelanggan=$_POST['nama_pelanggan'];
                          $alamat=$_POST['alamat'];
                          $id_tarif=$_POST['id_tarif'];
    
                          $sql=mysqli_query($koneksi, "UPDATE pelanggan set username='$username', password='$password', nomor_kwh='$nomor_kwh', nama_pelanggan='$nama_pelanggan', alamat='$alamat', id_tarif='$id_tarif' where id_pelanggan='$id_pelanggan'");
                          if($sql){
                            echo "<div class='alert alert-info'>Data Berhasil diedit</div>";
                            echo "<meta http-equiv='refresh' content='1;url=manage_user.php'>";
                          }else{
                            echo"Gagal";
                          }

                          }
                        ?>                       
                   </div>
               </div>
               <!-- /. ROW  -->
               <hr /> 
           </div>
           <!-- /. PAGE INNER  -->
       </div>
       <!-- /. PAGE WRAPPER  -->
   </div>
   <!-- /. WRAPPER  -->
   <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
   <!-- JQUERY SCRIPTS -->
   <script src="../assets/js/jquery-1.10.2.js"></script>
   <!-- BOOTSTRAP SCRIPTS -->
   <script src="../assets/js/bootstrap.min.js"></script>
   <!-- METISMENU SCRIPTS -->
   <script src="../assets/js/jquery.metisMenu.js"></script>
   <!-- CUSTOM SCRIPTS -->
   <script src="../assets/js/custom.js"></script>

   
</body>
</html>
