<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
  <?php 
  include 'koneksi.php';
  session_start();
  if (empty($_SESSION['username'])) {
    header('location:login.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
  }
  ?>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">E-PLN</a> 
      </div>
      <div style="color: white;
      padding: 15px 50px 5px 50px;
      float: right;
      font-size: 16px;"><a href="logout.php" class="btn btn-danger square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
    </nav> 

    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li class="text-center">
            <img src="img/log.png" class="user-image img-responsive"/>
            <p style="color: white; margin-top: -25px"><?php echo $pelanggan['nama_pelanggan'];?></p>
          </li>
          <li>
            <a class="active-menu" href="index.php"><i class="fa fa-home fa-2x"></i>Dashboard</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Riwayat<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="riwayat_pembayaran.php">Pembayaran</a>
              </li>
              <li>
                <a href="laporan_topup.php">Tagihan</a>
              </li>
              <li>
                <a href="riwayat_penggunaan.php">Penggunaan</a>
              </li>
            </ul>
          </li>     
        </ul>

      </div>

    </nav>  
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">  
           <h5>Welcome To E-PLN</h5>

         </div>

       </div>
       <!-- /. ROW  -->
       <hr />

       <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">
            <div class="panel-heading">
              Cek Tagihan
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <form action="proses_cek_tagihan.php" method="POST">
                    <div class="form-group">
                      <label>Nomor Kwh</label>
                      <input type="number" name="nomor_kwh" class="form-control" placeholder="Enter Nomor Kwh" value="<?php echo $pelanggan['nomor_kwh']; ?>" disabled>
                    </div>
                    <div class="form-group">
                      <label>Meter Akhir</label>
                      <input type="number" name="meter_akhir" class="form-control" placeholder="Enter meter akhir" required>
                      <small><i>*Silahkan masukan meter akhir</i></small>
                    </div>
                    <div class="form-group">
                      <input type="submit" name="cek_tagihan" value="Cek Tagihan" class="btn btn-primary">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>
    <!-- /. PAGE INNER  -->
  </div>
  <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>


</body>
</html>
