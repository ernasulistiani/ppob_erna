<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
  <?php
  include 'koneksi.php';
  session_start();
  if (empty($_SESSION['username'])) {
    header('location:login.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
  }
  ?>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">E-PLN</a> 
      </div>
      <div style="color: white;
      padding: 15px 50px 5px 50px;
      float: right;
      font-size: 16px;"><a href="logout.php" class="btn btn-danger square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
    </nav> 

    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li class="text-center">
            <img src="img/log.png" class="user-image img-responsive"/>
            <p style="color: white; margin-top: -25px"><?php echo $pelanggan['nama_pelanggan'];?></p>
          </li>
          <li>
            <a class="active-menu" href="index.php"><i class="fa fa-home fa-2x"></i>Dashboard</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Riwayat<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="riwayat_pembayaran.php">Pembayaran</a>
              </li>
              <li>
                <a href="laporan_topup.php">Tagihan</a>
              </li>
              <li>
                <a href="riwayat_penggunaan.php">Penggunaan</a>
              </li>
            </ul>
          </li>     
        </ul>

      </div>

    </nav>  
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">  
           <h4 align="center">Detail Tagihan</h4>

         </div>

       </div>
       <!-- /. ROW  -->
       <br />

       <?php
       include "koneksi.php";
$today = date("Ymd"); //untuk mengambil tahun, tanggal dan tahun hari ini
$tanggal_daftar = date("d/m/Y");
//cari id terakhir di tanggal hari ini
$query1 = "SELECT max(id_tagihan) as maxID FROM tagihan WHERE id_tagihan LIKE '$today%'";
$hasil = mysqli_query($koneksi, $query1);
$data = mysqli_fetch_array($hasil);
$idMax = $data['maxID'];

//setelah membaca id terakhir , lanjut mencari nomor urut id dari id terakhir
$NoUrut = (int) substr($idMax, 8, 3);
$NoUrut++; //nomor urut +1

//setelah ketemu id terakhir lanjut membuat id baru dengan format sbb:
$NewID = $today .sprintf('%03s',$NoUrut);
//$today nanti jadinya misal 20160526 .sprintf('%03s', $NoUrut) urutan id di tanggal hari ini

$id_pelanggan = $_SESSION['id_pelanggan'];
$query_penggunaan = mysqli_query($koneksi, "SELECT * FROM penggunaan WHERE id_pelanggan='$id_pelanggan'");
$penggunaan = mysqli_fetch_array($query_penggunaan);
$id_penggunaan = $penggunaan['id_penggunaan'];

//Penggambilan data di tabel pelanggan
$query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan'");
$pelanggan = mysqli_fetch_array($query_pelanggan);
$nama_pelanggan = $pelanggan['nama_pelanggan'];
$id_tarif = $pelanggan['id_tarif'];

//pengambilan data di tabel tarif berdasarkan id tarif pelanggan
$query_tarif = mysqli_query($koneksi, "SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
$tarif = mysqli_fetch_array($query_tarif);
$tarifperkwh = $tarif['tarifperkwh'];

$bulan_tagihan = $penggunaan['bulan'];
$tahun_tagihan = $penggunaan['tahun'];
$jumlah_meter_penggunaan = $penggunaan['meter_akhir'] - $penggunaan['meter_awal'];
$jumlah_tagihan = $jumlah_meter_penggunaan * $tarifperkwh;
$tahun_cek = date("Y");
$bulan_cek = date('n');

$tagihan_query = mysqli_query($koneksi, "SELECT * FROM tagihan where id_pelanggan='$id_pelanggan' and status='Belum DiBayar'");
$cek_tagihan = mysqli_num_rows($tagihan_query);
$tagihan = mysqli_fetch_array($tagihan_query);
if($cek_tagihan > 0) {
  echo "<script>window.alert('Masih ada tagihan yang belum dibayar')</script>";
  $status = $tagihan['status'];
}
else{
  mysqli_query($koneksi, "INSERT INTO tagihan values ('$NewID','$id_penggunaan','$id_pelanggan','$bulan_tagihan','$tahun_tagihan','$jumlah_meter_penggunaan','Belum Dibayar')");
  $status = "Belum DiBayar";
}
?>

<div class="row">
  <div class="col-md-12">
    <!-- Form Elements -->
    <div class="panel panel-default">
      <div class="panel-heading">
        Detail Tagihan
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <form action="proses_pembayaran.php?p=<?php echo $NewID ?>" method="POST">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Pelanggan</label>
                <div class="col-sm-9">
                  <input type="text" name="nama_pelanggan" class="form-control" value="<?php echo $pelanggan['nama_pelanggan']; ?>" placeholder="Id Pembayaran" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nomor KWH</label>
                <div class="col-sm-9">
                  <input type="number" name="nomor_kwh" value="<?php echo $pelanggan['nomor_kwh']; ?>" class="form-control" placeholder="Nama Pelanggan" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Meter Awal</label>
                <div class="col-sm-9">
                  <input type="number" name="meter_awal" value="<?php echo $penggunaan['meter_awal']; ?>" class="form-control" placeholder="Nama Pelanggan" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Meter Akhir</label>
                <div class="col-sm-9">
                  <input type="number" name="meter_akhir" value="<?php echo $penggunaan['meter_akhir']; ?>" class="form-control" placeholder="Nama Pelanggan" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Daya</label>
                <div class="col-sm-9">
                  <input type="number" name="daya" class="form-control" value="<?php echo $tarif['daya']; ?>" placeholder="No Tagihan" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Tarif Per-Kwh</label>
                <div class="col-sm-9">
                  <input type="number" name="tarifperkwh" class="form-control" value="<?php echo $tarif['tarifperkwh']; ?>" placeholder="No Pelanggan" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Bulan Tagihan</label>
                <div class="col-sm-9">
                  <input type="text" name="bulan" class="form-control" value="<?php echo $penggunaan['bulan']; ?> <?php echo $penggunaan['tahun']; ?>" placeholder="Biaya Admin Bank" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Jumlah Tagihan</label>
                <div class="col-sm-9">
                  <input type="number" name="jumlah_tagihan" class="form-control" value="<?php echo $jumlah_tagihan; ?>" placeholder="Total Bayar" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Status</label>
                <div class="col-sm-9">
                  <input type="text" name="status" class="form-control" value="<?php echo $status; ?>" placeholder="Total Bayar" readonly>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-3">
                  <input type="submit" class="btn btn-danger" name="bayar" value="Bayar Sekarang">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<hr />

</div>
<!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>


</body>
</html>
