<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="assets/js/dataTables/dataTables.bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
  <?php
  include 'koneksi.php'; 
  session_start();
  if (empty($_SESSION['username'])) {
    header('location:login.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
  }
  ?>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">E-PLN</a> 
      </div>
      <div style="color: white;
      padding: 15px 50px 5px 50px;
      float: right;
      font-size: 16px;"><a href="logout.php" class="btn btn-danger square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
    </nav> 

    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li class="text-center">
            <img src="img/log.png" class="user-image img-responsive"/>
            <p style="color: white; margin-top: -25px"><?php echo $pelanggan['nama_pelanggan'];?></p>
          </li>
          <li>
            <a href="index.php"><i class="fa fa-home fa-2x"></i>Dashboard</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Riwayat<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="riwayat_pembayaran.php">Pembayaran</a>
              </li>
              <li>
                <a href="laporan_topup.php">Tagihan</a>
              </li>
              <li>
                <a href="riwayat_penggunaan.php">Penggunaan</a>
              </li>
            </ul>
          </li>     
        </ul>

      </div>

    </nav>  
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">  
           <h4 align="center">Riwayat Pembayaran</h4>
           <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="example">
                <thead>
                  <tr>
                    <th>Tanggal Pembayaran</th>
                    <th>Bulan Bayar</th>
                    <th>Jumlah Bayar</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                  include 'koneksi.php';
                  $query_pembayaran = mysqli_query($koneksi, "SELECT * FROM pembayaran WHERE id_pelanggan='$_SESSION[id_pelanggan]'");
                    while ($pembayaran=mysqli_fetch_array($query_pembayaran)) { ?>
                    <tr>
                      <td><?php echo $pembayaran['tanggal_pembayaran']; ?></td>
                      <td>
                        <?php
                        switch ($pembayaran['bulan_bayar']) {
                          case "1";
                            $bulan_bayar = "Januari";
                          break;
                          case "2";
                            $bulan_bayar = "Februari";
                          break;
                          case "3";
                            $bulan_bayar = "Maret";
                          break;
                          case "4";
                            $bulan_bayar = "April";
                          break;
                          case "5";
                            $bulan_bayar = "Mei";
                          break;
                          case "6";
                            $bulan_bayar = "Juni";
                          break;
                          case "7";
                            $bulan_bayar = "Juli";
                          break;
                          case "8";
                            $bulan_bayar = "Agustus";
                          break;
                          case "9";
                            $bulan_bayar = "September";
                          break;
                          case "10";
                            $bulan_bayar = "Oktober";
                          break;
                          case "11";
                            $bulan_bayar = "November";
                          break;
                          case "12";
                            $bulan_bayar = "Desember";
                          break;
                        }
                        echo $bulan_bayar; ?>
                      </td>
                      <td><?php echo $pembayaran['jumlah_bayar']; ?></td>
                      <td>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#detail_pembayaran-<?php echo $pembayaran['id_pembayaran'];?>">Detail</button>
                      </td>
                    </tr>

                    <?php
                    //Penggambilan data di tabel pelanggan
                    $id_pelanggan = $_SESSION['id_pelanggan'];
                    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan'");
                    $pelanggan = mysqli_fetch_array($query_pelanggan);
                    $nama_pelanggan = $pelanggan['nama_pelanggan'];
                    ?>

                    <!-- Modal -->
                    <div class="modal fade" id="detail_pembayaran-<?php echo $pembayaran['id_pembayaran'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Detail Pembayaran</h4>
                          </div>
                          <div class="modal-body">
                            <form action="" method="POST">
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Pelanggan</label>
                                <div class="col-sm-8">
                                  <input type="text" name="nama_pelanggan" class="form-control" placeholder="Nama Pelanggan" value="<?php echo $pelanggan['nama_pelanggan']; ?>" readonly>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nomor KWH</label>
                                <div class="col-sm-8">
                                  <input type="text" name="nomor_kwh" class="form-control" value="<?php echo $pelanggan['nomor_kwh']; ?>" placeholder="Nomor kwh" readonly>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Tanggal Pembayaran</label>
                                <div class="col-sm-8">
                                  <input type="text" name="tanggal_pembayaran" class="form-control" placeholder="Tanggal Pembayaran" value="<?php echo $pembayaran['tanggal_pembayaran']; ?>" readonly>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Bulan Bayar</label>
                                <div class="col-sm-8">
                                  <input type="text" name="bulan_bayar" class="form-control" placeholder="Bulan Bayar" value="<?php echo $bulan_bayar; ?>" readonly>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Jumlah Bayar</label>
                                <div class="col-sm-8">
                                  <input type="number" name="jumlah_bayar" class="form-control" placeholder="Jumlah Bayar" value="<?php echo $pembayaran['jumlah_bayar']; ?>" readonly>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Biaya Admin</label>
                                <div class="col-sm-8">
                                  <input type="number" name="biaya_admin" class="form-control" placeholder="Biaya Admin" value="<?php echo $pembayaran['biaya_admin']; ?>" readonly>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Total Bayar</label>
                                <div class="col-sm-8">
                                  <input type="text" name="total_bayar" class="form-control" placeholder="Total Bayar" value="<?php echo $pembayaran['total_bayar']; ?>" readonly>
                                </div>
                              </div>
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-danger">Cetak Struk</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <?php } ?>

                  </tbody>
                </table>
              </div>
            </div>

          </div>

        </div>
        <!-- /. ROW  -->
        <hr />




      </div>
      <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
  </div>
  <!-- /. WRAPPER  -->
  <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
  <!-- JQUERY SCRIPTS -->
  <script src="assets/js/jquery-1.10.2.js"></script>
  <!-- BOOTSTRAP SCRIPTS -->
  <script src="assets/js/bootstrap.min.js"></script>
  <!-- METISMENU SCRIPTS -->
  <script src="assets/js/jquery.metisMenu.js"></script>
  <!-- DATA TABLE SCRIPTS -->
  <script src="assets/js/dataTables/jquery.dataTables.js"></script>
  <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
  <!-- CUSTOM SCRIPTS -->
  <script>
    $(document).ready(function () {
      $('#example').dataTable();
    });
  </script>
  <!-- CUSTOM SCRIPTS -->
  <script src="assets/js/custom.js"></script>


</body>
</html>
